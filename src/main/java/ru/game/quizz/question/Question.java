package ru.game.quizz.question;

import ru.game.quizz.answer.Answer;
import ru.game.quizz.answer.CorrectAnswer;
import ru.game.quizz.answer.IncorrectAnswer;

import java.util.List;

class Question {
    private String text;
    private List<Answer> answers;

    public Question(String text, CorrectAnswer correctAnswer) {
        this.text = text;
        this.answers.add(correctAnswer);
    }

    public void addAnswer(IncorrectAnswer answer) {
        this.answers.add(answer);
    }

    public boolean checkAnswer(Answer answer) {
        return this.getCorrectAnswer().isEqualTo(answer);
    }

    private Answer getCorrectAnswer() {
       return (Answer) answers.stream().filter(Answer::isCorrect);
    }
}