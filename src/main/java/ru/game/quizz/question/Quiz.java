package ru.game.quizz.question;

import java.util.List;

class Quiz {

    private List<Question> questions;

    public Question nextQuestion() {
        return this.questions.iterator().next();
    }
}