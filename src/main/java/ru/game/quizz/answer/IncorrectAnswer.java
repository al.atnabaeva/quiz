package ru.game.quizz.answer;

public class IncorrectAnswer extends Answer {

    public IncorrectAnswer(String text) {
        super(text);
    }

    public boolean isCorrect() {
        return false;
    }
}