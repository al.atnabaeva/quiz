package ru.game.quizz.answer;

public abstract class Answer {
    private final String text;
    public Answer (String text) {
        this.text = text;
    }

    public boolean isEqualTo(Answer answer) {
        return this.text.equals(answer.text);
    }

    public String getText() {
        return this.text;
    }
    abstract public boolean isCorrect();
}