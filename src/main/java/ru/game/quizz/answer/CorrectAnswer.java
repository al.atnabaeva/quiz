package ru.game.quizz.answer;

public class CorrectAnswer extends Answer {

    public CorrectAnswer(String text) {
        super(text);
    }

    public boolean isCorrect() {
        return true;
    }
}