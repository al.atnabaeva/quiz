package ru.game.quizz.square;

public abstract class SquareDecorator implements GameField {
    GameField square;

    public SquareDecorator(GameField square) {
        this.square = square;
    }
}