package ru.game.quizz.square;

public class OreSquareDecorator extends SquareDecorator {

    public OreSquareDecorator(GameField square) {
        super(square);
    }

    @Override
    public int getGold() {
        return this.square.getGold() + 3;
    }
}