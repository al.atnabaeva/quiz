package ru.game.quizz.square;

public class DirtySquareDecorator extends SquareDecorator {

    public DirtySquareDecorator(GameField square) {
        super(square);
    }

    @Override
    public int getGold() {
        return this.square.getGold() - 2;
    }
}