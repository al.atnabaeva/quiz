package ru.game.quizz.square;

public class EmptySquareDecorator extends SquareDecorator {

    public EmptySquareDecorator(GameField square) {
        super(square);
    }

    @Override
    public int getGold() {
        return this.square.getGold() + 1;
    }
}