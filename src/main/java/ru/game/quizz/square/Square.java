package ru.game.quizz.square;

public class Square implements GameField {

    private boolean empty = false;
    private boolean ore = false;
    private boolean dirty = false;

    public boolean getEmpty() {
        return empty;
    }

    public Square setEmpty(boolean empty) {
        this.empty = empty;
        return this;
    }

    public boolean getOre() {
        return ore;
    }

    public Square setOre(boolean ore) {
        this.ore = ore;
        return this;
    }

    public boolean getDirty() {
        return dirty;
    }

    public Square setDirty(boolean dirty) {
        this.dirty = dirty;
        return this;
    }

    @Override
    public int getGold() {
        return 0;
    }
}