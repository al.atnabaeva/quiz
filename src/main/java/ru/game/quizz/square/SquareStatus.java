package ru.game.quizz.square;

public class SquareStatus {

    private boolean empty = false;
    private boolean ore = false;
    private boolean dirty = false;

    public boolean getEmpty() {
        return empty;
    }

    public SquareStatus setEmpty(boolean empty) {
        this.empty = empty;
        return this;
    }

    public boolean getOre() {
        return ore;
    }

    public SquareStatus setOre(boolean ore) {
        this.ore = ore;
        return this;
    }

    public boolean getDirty() {
        return dirty;
    }

    public SquareStatus setDirty(boolean dirty) {
        this.dirty = dirty;
        return this;
    }
}