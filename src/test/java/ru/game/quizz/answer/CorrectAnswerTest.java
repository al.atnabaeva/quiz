package ru.game.quizz.answer;

import org.junit.Test;

import static org.junit.Assert.*;

public class CorrectAnswerTest {

    @Test
    public void isCorrect() {
        String text = "testAnswer";
        CorrectAnswer answer = new CorrectAnswer(text);
        assertEquals(answer.getText(), text);
        assertTrue(answer.isCorrect());
    }
}